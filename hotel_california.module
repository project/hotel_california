<?php

/**
 * @file
 * A module to prevent users from logging out.
 */

/**
 * Implements hook_menu().
 */
function hotel_california_menu() {
  $items = array();

  $items['admin/config/people/hotel_california'] = array(
    'title' => 'Hotel California',
    'description' => 'Redirect attempted logout requests',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hotel_california_admin'),
    'access arguments' => array('Administer site configuration'),
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function hotel_california_menu_alter(&$items) {
  if (strlen(variable_get('hotel_california_logout_path', '')) <= 0) {
    unset($items['user/logout']);
  }
  else {
    unset($items['user/logout']['file']);
    $items['user/logout']['page callback'] = '_hotel_california_user_logout';
  }
}

/**
 * Redirects a user to a different logout page.
 */
function _hotel_california_user_logout() {
  drupal_goto(
    variable_get('hotel_california_logout_path', ''),
    array(),
    variable_get('hotel_california_logout_response', 301)
  );
}

/**
 * Implements function hook_url_inbound_alter().
 */
function hotel_california_url_inbound_alter(&$path, $original_path, $path_language) {
  if ($path === 'user/logout') {
    variable_get('hotel_california_logout_path', 'user');
  }
}

/**
 * Path configuration form.
 */
function hotel_california_admin() {
  $form = array();

  $form['hotel_california_logout_path'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Logout path'),
      '#default_value'  => variable_get('hotel_california_logout_path', ''),
      '#required'       => TRUE,
    );

  $form['hotel_california_logout_response'] = array(
    '#type' => 'select',
    '#title' => t('HTTP status code'),
    '#options' => array(
      301 => '301: ' . t('Moved Permanently (the recommended value for most redirects).'),
      302 => '302: ' . t('Found (default in Drupal and PHP, sometimes used for spamming search engines).'),
      303 => '303: ' . t('See Other.'),
      304 => '304: ' . t('Not Modified.'),
      305 => '305: ' . t('Use Proxy.'),
      307 => '307: ' . t('Temporary Redirect.'),
    ),
    '#default_value' => variable_get('hotel_california_logout_response', 301),
    '#description' => t('The HTTP status code to use for the redirection.'),
  );

  return system_settings_form($form);
}

